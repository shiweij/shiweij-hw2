from mininet.net import Mininet
from mininet.node import Controller, RemoteController, Node
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, Intf
from mininet.topo import Topo
    
def aggNet():

    net = Mininet( topo=None, controller=Controller, build=False)
    
    c1 = net.addController('c1', controller=RemoteController, ip='127.0.0.1', port=6633)
    s1 = net.addSwitch('s1')
    
    h1 = net.addHost('h1', ip='0.0.0.0')
    h2 = net.addHost('h2', ip='0.0.0.0')
    h3 = net.addHost('h3', ip='0.0.0.0')
    
    net.addLink(s1,h1)
    net.addLink(s1,h2)
    net.addLink(s1,h3)

#    net.start()        
    net.build()
    c1.start()
    s1.start([c1])
    CLI( net )
    net.stop()
 
if __name__ == '__main__':
    setLogLevel( 'info' )
    aggNet()
